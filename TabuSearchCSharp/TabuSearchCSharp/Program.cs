﻿using System;
using TabuSearchCSharp.Model;
using TabuSearchCSharp.Application;

namespace TabuSearchCSharp
{
    class Program
    {
        // Arguments:
        // arg[0] -> File path for a TSPLib instance with NODE_COORD_SECTION section
        //      TSPLib: http://comopt.ifi.uni-heidelberg.de/software/TSPLIB95/
        // arg[1] -> Tabu list size (tenure)
        // arg[2] -> Execution limit, in seconds
        // Optional arguments:
        //      --firstImprovement: Include to use first improvement strategy. Default is best improviment.
        static void Main(string[] args)
        {
            if (args.Length < 3)
            {
                Console.WriteLine("Missing arguments");
                return;
            }

            string filePath = args[0];
            int timeLimit;
            int tenure;

            if (!int.TryParse(args[1], out tenure) ||
                !int.TryParse(args[2], out timeLimit))
            {
                Console.WriteLine("Failed to parse parameter");
                return;
            }

            ImprovementType improvementType = ImprovementType.BestImproviment;
            foreach(string arg in args)
            {
                if (arg.Equals("--firstImprovement"))
                {
                    improvementType = ImprovementType.FirstImprovement;
                }
            }
            
            TSPInstance instance = new TSPInstance(filePath);

            if (instance.GetPoints().Count == 0)
            {
                Console.WriteLine("Empty instance");
                return;
            }

            TSPSolution solution = new TSPSolution(instance);
            Console.WriteLine("Initial solution");
            solution.PrintSolution();

            TabuSolver solver = new TabuSolver(tenure, solution, improvementType, timeLimit);
            solver.Solve();
            Console.WriteLine("Best Solution: ");
            solver.BestSolution.PrintSolution();
        }
    }
}
