﻿namespace TabuSearchCSharp.Model
{
    public abstract class BasePoint
    {
        abstract public double Distance(BasePoint toPoint);
        public int Index { get; private set; }
        public int X { get; private set; }
        public int Y { get; private set; }

        protected BasePoint(int x, int y, int index)
        {
            X = x;
            Y = y;
            Index = index;
        }
    }
}
