﻿using System.Collections.Generic;

namespace TabuSearchCSharp.Model
{
    public interface IInstance<T>
    {
        double[,] GetDistanceMatrix();
        List<T> GetPoints();
    }
}
