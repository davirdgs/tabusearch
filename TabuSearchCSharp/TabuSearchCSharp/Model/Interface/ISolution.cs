﻿using System.Collections.Generic;
namespace TabuSearchCSharp.Model
{
    public interface ISolution<T>
    {
        IInstance<T> GetInstance();
        List<T> GetPermutation();
        double GetCost();
        void PrintSolution();
    }
}
