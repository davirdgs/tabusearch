﻿using System;

namespace TabuSearchCSharp.Model
{
    public class Point : BasePoint
    {
        public Point(int x, int y, int index) : base(x, y, index) { }

        public override double Distance(BasePoint toPoint)
        {
            double xa = Convert.ToDouble(X);
            double ya = Convert.ToDouble(Y);
            double xb = Convert.ToDouble(toPoint.X);
            double yb = Convert.ToDouble(toPoint.Y);
            return Math.Sqrt((xa - xb) * (xa - xb) + (ya - yb) * (ya - yb));
        }
    }
}
