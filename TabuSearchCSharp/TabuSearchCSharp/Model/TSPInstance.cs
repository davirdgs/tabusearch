﻿using System.Collections.Generic;
using TabuSearchCSharp.Application;

namespace TabuSearchCSharp.Model
{
    public class TSPInstance : IInstance<BasePoint>
    {
        private double[,] DistanceMatrix;
        private readonly List<BasePoint> Points;

        public TSPInstance(string filepath)
        {
            Points = FileHandler.ReadPointsFromFile(filepath);
            SetDistanceMatrix();
        }

        public double[,] GetDistanceMatrix()
        {
            return DistanceMatrix;
        }

        public List<BasePoint> GetPoints()
        {
            return Points;
        }

        private void SetDistanceMatrix()
        {
            DistanceMatrix = new double[Points.Count, Points.Count];

            for (int i = 0; i < Points.Count; i++)
            {
                for (int j = 0; j < Points.Count; j++)
                {
                    if (i == j)
                    {
                        DistanceMatrix[i,j] = 0;
                        continue;
                    }
                    DistanceMatrix[i, j] = Points[i].Distance(Points[j]);
                    DistanceMatrix[j, i] = Points[i].Distance(Points[j]);
                }
            }
        }
    }
}
