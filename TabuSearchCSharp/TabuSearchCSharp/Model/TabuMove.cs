﻿namespace TabuSearchCSharp.Model
{
    public struct TabuMove<T>
    {
        public ISolution<T> Solution { get; private set; }
        public int ElemA { get; private set; }
        public int ElemB { get; private set; }

        public TabuMove(ISolution<T> solution,
            int elemA,
            int elemB)
        {
            Solution = solution;
            ElemA = elemA;
            ElemB = elemB;
        }
    }
}
