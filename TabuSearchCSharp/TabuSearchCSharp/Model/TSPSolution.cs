﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TabuSearchCSharp.Model
{
    public class TSPSolution : ISolution<BasePoint>
    {

        private List<BasePoint> Permutation;
        private IInstance<BasePoint> Instance;
        private double Cost;

        public TSPSolution(List<BasePoint> permutation, IInstance<BasePoint> instance)
        {
            Permutation = permutation;
            Instance = instance;
            CalculateCost();
        }

        // Creates a random solution
        public TSPSolution(IInstance<BasePoint> instance)
        {
            Instance = instance;
            Permutation = instance.GetPoints();
            RandomizePermutation();
            CalculateCost();
        }

        public double GetCost()
        {
            return Cost;
        }

        public IInstance<BasePoint> GetInstance()
        {
            return Instance;
        }

        public List<BasePoint> GetPermutation()
        {
            return Permutation;
        }

        public void PrintSolution()
        {
            Console.WriteLine("Solution Cost: " + Cost);
            foreach(Point point in Permutation)
            {
                Console.Write(point.Index + " ");
            }
            Console.Write("\n\n");
        }

        private void RandomizePermutation()
        {
            Random rng = new Random();
            int permutationSize = Permutation.Count;
            foreach (int i in Enumerable.Range(0, permutationSize))
            {
                int index = rng.Next(permutationSize);
                BasePoint p = Permutation[i];
                Permutation[i] = Permutation[index];
                Permutation[index] = p;
            }
        }

        private void CalculateCost()
        {
            if (Permutation.Count < 2)
            {
                Cost = 0;
                return;
            }

            double[,] distanceMatrix = Instance.GetDistanceMatrix();
            for (int i = 0; i < Permutation.Count; i++)
            {
                if (i == Permutation.Count - 1)
                {
                    Cost += distanceMatrix[Permutation[i].Index, Permutation[0].Index];
                } else
                {
                    Cost += distanceMatrix[Permutation[i].Index, Permutation[i+1].Index];
                }
            }
        }
    }
}
