﻿using System.Collections.Generic;
using TabuSearchCSharp.Model;

namespace TabuSearchCSharp.Application
{
    internal interface IOperation
    {
        ISolution<BasePoint> GetMove(ISolution<BasePoint> solution, int position, int toPosition);
    }

    internal class OneExchangeOperation : IOperation
    {
        public ISolution<BasePoint> GetMove(ISolution<BasePoint> solution, int position, int toPosition)
        {
            List<BasePoint> permutation = solution.GetPermutation();
            BasePoint point = permutation[position];
            permutation.RemoveAt(position);
            permutation.Insert(toPosition, point);

            TSPSolution _solution = new TSPSolution(permutation, solution.GetInstance());
            return _solution;
        }
    }

    internal class TwoExchangeOperation : IOperation
    {
        public ISolution<BasePoint> GetMove(ISolution<BasePoint> solution, int position, int toPosition)
        {
            List<BasePoint> permutation = solution.GetPermutation();
            BasePoint pointA = permutation[position];
            BasePoint pointB = permutation[toPosition];
            permutation[position] = pointB;
            permutation[toPosition] = pointA;

            TSPSolution _solution = new TSPSolution(permutation, solution.GetInstance());
            return _solution;
        }
    }
}
