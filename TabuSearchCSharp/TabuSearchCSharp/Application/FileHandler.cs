﻿using System;
using TabuSearchCSharp.Model;
using System.Collections.Generic;

namespace TabuSearchCSharp.Application
{
    internal struct FileHandler
    {

        internal static List<BasePoint> ReadPointsFromFile(string filepath)
        {
            List<BasePoint> points = new List<BasePoint>();
            bool waitPoint = false;
            if (!System.IO.File.Exists(filepath))
            {
                Console.WriteLine("Invalid filepath");
                return points;
            }
            string[] lines = System.IO.File.ReadAllLines(filepath);

            foreach (string line in lines)
            {
                char[] separators = { ' ' };
                string[] tokens = line.Split(separators);

                if (waitPoint)
                {
                    if (tokens.Length != 3 || tokens[0].Equals("EOF"))
                    {
                        break;
                    }

                    int x, y, index;
                    if (!int.TryParse(tokens[0], out index) ||
                        !int.TryParse(tokens[1], out x) ||
                        !int.TryParse(tokens[2], out y))
                    {
                        Console.WriteLine("Failed to parse number");
                        break;
                    }
                    // On TSPLib, index starts at 1
                    Point point = new Point(x, y, index - 1);
                    points.Add(point);
                }

                // Parse only NODE_COORD_SECTION content
                if (tokens[0].Equals("NODE_COORD_SECTION"))
                {
                    waitPoint = true;
                }
            }

            return points;
        }
    }
}
