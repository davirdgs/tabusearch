﻿using TabuSearchCSharp.Model;
using System.Collections.Generic;
using System.Diagnostics;
using System;

namespace TabuSearchCSharp.Application
{
    internal enum ImprovementType
    {
        FirstImprovement,
        BestImproviment
    }

    internal class TabuSolver
    {
        // Dependency inversion principle: Entities must depend on abstractions, not on concretions
        private List<TabuMove<BasePoint>> CandidateList;
        private List<int> TabuList;
        private ISolution<BasePoint> IncubentSolution;
        public ISolution<BasePoint> BestSolution { get; private set; }
        private ImprovementType Improvement;
        private int Tenure;
        private int ExecutionTime; // In seconds

        internal TabuSolver(int tenure, ISolution<BasePoint> initialSolution, ImprovementType improvement, int executionTime)
        {
            Tenure = tenure;
            BestSolution = initialSolution;
            IncubentSolution = initialSolution;
            CandidateList = new List<TabuMove<BasePoint>>();
            TabuList = new List<int>();
            Improvement = improvement;
            ExecutionTime = executionTime;
        }

        internal void Solve()
        {
            Console.WriteLine("Starting solver with " + Improvement.ToString());
            Console.WriteLine("Tenure " + Tenure + "\n");
            Stopwatch timer = new Stopwatch();
            timer.Start();
            while (timer.Elapsed < TimeSpan.FromSeconds(ExecutionTime))
            {
                GetCandidateList();
                TabuMove<BasePoint> nextMove = GetBestMove();
                PerformMove(nextMove);
                UpdateTabuList(nextMove.ElemA);
                UpdateTabuList(nextMove.ElemB);
            }
        }

        private TabuMove<BasePoint> GetBestMove()
        {
            if (CandidateList.Count == 0)
            {
                throw new Exception("CandidateList Empty!");
            }

            TabuMove<BasePoint> bestMove = CandidateList[0];
            double bestCost = bestMove.Solution.GetCost();
            foreach (TabuMove<BasePoint> move in CandidateList)
            {
                if(move.Solution.GetCost() < bestCost)
                {
                    bestMove = move;
                    bestCost = move.Solution.GetCost();

                    // First improviment: stop execution as soon as a better solution is found
                    if (Improvement == ImprovementType.FirstImprovement &&
                        move.Solution.GetCost() < IncubentSolution.GetCost())
                    {
                        return bestMove;
                    }
                }
            }

            return bestMove;
        }

        private void PerformMove(TabuMove<BasePoint> move)
        {
            IncubentSolution = move.Solution;
            if (IncubentSolution.GetCost() < BestSolution.GetCost())
            {
                BestSolution = IncubentSolution;
                IncubentSolution.PrintSolution();
            }
        }

        private void GetCandidateList()
        {
            // Reset candidate list
            CandidateList = new List<TabuMove<BasePoint>> ();

            for (int i = 0; i < IncubentSolution.GetPermutation().Count; i++)
            {
                for (int j = 0; j < IncubentSolution.GetPermutation().Count; j++)
                {
                    if (i == j) continue; // Trivial move
                    if (TabuList.Contains(i) || TabuList.Contains(j)) continue; // Tabu move

                    // Get all 1exchange moves
                    TabuMove<BasePoint> oneExMove = GetOneExchangeMove(i, j);
                    CandidateList.Add(oneExMove);

                    // Get all 2exchange moves
                    // Exchanging i and j is the same as exchanging j and i
                    if (i > j)
                    {
                        TabuMove<BasePoint> twoExMove = GetTwoExchangeMove(i, j);
                        CandidateList.Add(twoExMove);
                    }
                }
            }
        }

        private TabuMove<BasePoint> GetOneExchangeMove(int i, int j)
        {
            IOperation operation = new OneExchangeOperation();
            ISolution<BasePoint> solution = operation.GetMove(IncubentSolution, i, j);
            TabuMove<BasePoint> newMove = new TabuMove<BasePoint>(solution, i, j);
            return newMove;
        }

        private TabuMove<BasePoint> GetTwoExchangeMove(int i, int j)
        {
            IOperation operation = new TwoExchangeOperation();
            ISolution<BasePoint> solution = operation.GetMove(IncubentSolution, i, j);
            TabuMove<BasePoint> newMove = new TabuMove<BasePoint>(solution, i, j);
            return newMove;
        }

        private void UpdateTabuList(int index)
        {
            if (TabuList.Count >= Tenure)
            {
                TabuList.RemoveAt(0);
            }
            TabuList.Add(index);
        }
    }
}
